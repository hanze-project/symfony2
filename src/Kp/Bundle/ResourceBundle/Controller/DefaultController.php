<?php

namespace Kp\Bundle\ResourceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('KpResourceBundle:Default:index.html.twig', array('name' => $name));
    }
}
