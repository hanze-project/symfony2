<?php

namespace Kp\Bundle\TagBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('KpTagBundle:Default:index.html.twig', array('name' => $name));
    }
}
