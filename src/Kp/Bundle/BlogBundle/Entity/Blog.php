<?php


namespace Kp\Bundle\BlogBundle\Entity;

use Kp\Bundle\BlogBundle\Model\Blog as BaseBlog;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Storage agnostic blog object
 *
 * @author Kevin Postma <kevinpostma@live.nl>
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="kp_user")
 */
class Blog extends BaseBlog
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
}

?>
