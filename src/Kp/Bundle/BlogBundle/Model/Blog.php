<?php

namespace Kp\Bundle\BlogBundle\Model;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use DoctrineExtensions\Taggable\Taggable;

/**
 * Storage agnostic blog object
 *
 * @author Kevin Postma <kevinpostma@live.nl>
 */
abstract class Blog implements Taggable
{
    protected $id;
    
    /**
     * @var boolean
     */
    protected $active;
    
    /**
     * @var boolean
     */
    protected $author;
    
    /**
     * @var \DateTime
     */
    protected $createdAt;
    
    /**
     * @var string
     */
    protected $category;
    
    /**
     * @var string
     */
    protected $title;
    
    /**
     * @var string
     */
    protected $blog;
    
    /**
     * @var array
     */
    protected $tags;
    
    public function getTags()
    {
        $this->tags = $this->tags ?: new ArrayCollection();

        return $this->tags;
    }

    public function getTaggableType()
    {
        return 'kp_tag';
    }

    public function getTaggableId()
    {
        return $this->getId();
    }
}

?>
