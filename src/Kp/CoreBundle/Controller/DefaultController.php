<?php

namespace Kp\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('KpCoreBundle:Default:index.html.twig', array('name' => $name));
    }
}
