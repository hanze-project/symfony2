<?php

namespace Kp\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('KpWebBundle:Frontend:index.html.twig');
    }
}
